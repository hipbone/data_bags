#!/bin/bash

file=$1

cd /game/chef-repo/data_bags/config

cat <<EOF > $file".json"
{
    "id" : "$file",
    "path" : "/usr/libexec/openssh/$file",
    "owner" : "root",
    "group" : "root",
    "mode" : "700"
}
EOF

data-bag-config-up $file".json"
